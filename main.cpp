/* Motor1 (M1) : Motor_Right
 * Motor2 (M2) : Motor_Left
 * Motor3 (M3) : Motor_Backward
 *
 * Ultrasonic_Front    : 22
 * Ultrasonic_Right    : 23
 * Ultrasonic_Left     : 24
 * Ultrasonic_Backward : 25
 */
#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_BNO055.h"
#include "utility/imumaths.h"
#include "SoftwareSerial.h"
#include "NewPing.h"
#include "Arduino.h"

Adafruit_BNO055 bno = Adafruit_BNO055(55);

/* Pin definitions */
SoftwareSerial mySerial(14,15); // RX, TX

#define SONAR_NUM 4      // Number of sensors.
#define MAX_DISTANCE 200 // Maximum distance (in cm) to ping.

NewPing sonar[SONAR_NUM] = {  // Sensor object array.
  NewPing(22, 22, MAX_DISTANCE), // Each sensor's trigger pin, echo pin, and max distance to ping.
  NewPing(23, 23, MAX_DISTANCE),
  NewPing(24, 24, MAX_DISTANCE),
  NewPing(25, 25, MAX_DISTANCE)
};

#define M1_ENCODER 2
#define M2_ENCODER 3
#define M3_ENCODER 18
#define M1_IN1 26
#define M1_IN2 27
#define M1_PWM 4
#define M2_IN1 28
#define M2_IN2 29
#define M2_PWM 5
#define M3_IN1 30
#define M3_IN2 31
#define M3_PWM 6
#define SWITCH_PIN_1 32
#define SWITCH_PIN_2 33

#define MAX_PWM 200

/* Function prototypes */
void motor_right(unsigned char motor_speed, bool motor_direction);
void motor_left(unsigned char motor_speed, bool motor_direction);
void motor_backward(unsigned char motor_speed, bool motor_direction);
void stopRobot(void);
void rightEncoderEvent(void);
void leftEncoderEvent(void);
void backwardEncoderEvent(void);
//void go(char dir);
void omniControl(unsigned char robot_speed, char dir);
void moveForward(unsigned char robot_speed);
void moveRight(unsigned char robot_speed);
void moveLeft(unsigned char robot_speed);
void moveBackward(unsigned char robot_speed);
void moveClockwise(unsigned char robot_speed);
void moveCounterClockwise(unsigned char robot_speed);

void centerRobot(unsigned char robot_speed);

/* Variable declarations */
float motor1_speed, motor2_speed, motor3_speed;

volatile unsigned long rightCount = 0;
volatile unsigned long rightPeriod = 0;
volatile unsigned long rightPrevTime = 0;

volatile unsigned long leftCount = 0;
volatile unsigned long leftPeriod = 0;
volatile unsigned long leftPrevTime = 0;

volatile unsigned long backwardCount = 0;
volatile unsigned long backwardPeriod = 0;
volatile unsigned long backwardPrevTime = 0;

// Motor1 and Motor2 PID parameters
float Kp_ = 400;
float Kd_ = 20;
float Ki_ = 0;
float error_ = 0;
float proportional_, derivative_, integral_, last_proportional_;

// Motor1 and Motor3 PID parameters
float Kp__ = 400;
float Kd__ = 20;
float Ki__ = 0;
float error__ = 0;
float proportional__, derivative__, integral__, last_proportional__;

// Motor2 and Motor3 PID parameters
float Kp___ = 400;
float Kd___ = 20;
float Ki___ = 0;
float error___ = 0;
float proportional___, derivative___, integral___, last_proportional___;

void setup()
{
  Serial.begin(9600);
  mySerial.begin(9600);

  /* Initialise the IMU sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }

  delay(1000);

  pinMode(M1_IN1,OUTPUT);
  pinMode(M1_IN2,OUTPUT);
  pinMode(M1_PWM,OUTPUT);
  pinMode(M2_IN1,OUTPUT);
  pinMode(M2_IN2,OUTPUT);
  pinMode(M2_PWM,OUTPUT);
  pinMode(M3_IN1,OUTPUT);
  pinMode(M3_IN2,OUTPUT);
  pinMode(M3_PWM,OUTPUT);

  pinMode(M1_ENCODER, INPUT_PULLUP);
  pinMode(M2_ENCODER, INPUT_PULLUP);
  pinMode(M3_ENCODER, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(M1_ENCODER), rightEncoderEvent, CHANGE);
  attachInterrupt(digitalPinToInterrupt(M2_ENCODER), leftEncoderEvent, CHANGE);
  attachInterrupt(digitalPinToInterrupt(M3_ENCODER), backwardEncoderEvent, CHANGE);

  pinMode(SWITCH_PIN_1, INPUT_PULLUP);
  pinMode(SWITCH_PIN_2, INPUT_PULLUP);
}

void loop()
{
  //omniControl(100,'F');

  /* Get a new sensor event */
  sensors_event_t event;
  bno.getEvent(&event);

  /* Display the floating point data */
  Serial.print("X: ");
  Serial.print(event.orientation.x, 0); //Serial.println(1.23456, 4) gives "1.2346"
  Serial.print("\tY: ");

  Serial.print(event.orientation.y, 4);
  Serial.print("\tZ: ");
  Serial.print(event.orientation.z, 4);
  Serial.println("");

  delay(250);
}
// Speed: from 0 to 255
// Direction: 1:Forward, 0:Backward
void motor_right(unsigned char robot_speed, bool robot_direction)
{
 if(!robot_direction){
  digitalWrite(M1_IN1,LOW);
  digitalWrite(M1_IN2,HIGH);
  }
 else
 {
  digitalWrite(M1_IN1,HIGH);
  digitalWrite(M1_IN2,LOW);
 }
 analogWrite(M1_PWM,robot_speed);
}

void motor_left(unsigned char robot_speed, bool robot_direction)
{
 if(!robot_direction){
  digitalWrite(M2_IN1,LOW);
  digitalWrite(M2_IN2,HIGH);
  }
 else
 {
  digitalWrite(M2_IN1,HIGH);
  digitalWrite(M2_IN2,LOW);
 }
 analogWrite(M2_PWM,robot_speed);
}

void motor_backward(unsigned char robot_speed, bool robot_direction)
{
 if(!robot_direction){
  digitalWrite(M3_IN1,LOW);
  digitalWrite(M3_IN2,HIGH);
  }
 else
 {
  digitalWrite(M3_IN1,HIGH);
  digitalWrite(M3_IN2,LOW);
 }
 analogWrite(M3_PWM,robot_speed);
}

void stopRobot(void)
{
 for(int i=0; i<50; i++)
 {
  motor_left(0,1);
  motor_right(0,1);
  motor_backward(0,1);
 }
  digitalWrite(M1_IN1,LOW);
  digitalWrite(M1_IN2,LOW);
  analogWrite(M1_PWM,0);
  digitalWrite(M2_IN1,LOW);
  digitalWrite(M2_IN2,LOW);
  analogWrite(M2_PWM,0);
  digitalWrite(M3_IN1,LOW);
  digitalWrite(M3_IN2,LOW);
  analogWrite(M3_PWM,0);
 for(int i=0; i<50; i++)
 {
  motor_left(0,0);
  motor_right(0,0);
  motor_backward(0,0);
 }
}

void rightEncoderEvent(void)
{
  rightCount++;
  //Serial.print("Right count: ");
  //Serial.println(rightCount);
  /*
  rightPeriod = micros() - rightPrevTime;
  Serial.print("Right period: ");
  Serial.println(rightPeriod);
  rightPrevTime = micros();
  */
}

void leftEncoderEvent(void)
{
  leftCount++;
  //Serial.print("Left count: ");
  //Serial.println(leftCount);
  /*
  leftPeriod = micros() - leftPrevTime;
  Serial.print("Left period: ");
  Serial.println(leftPeriod);
  leftPrevTime = micros();
  */
}

void backwardEncoderEvent(void)
{
  backwardCount++;
  //Serial.print("Backward count: ");
  //Serial.println(backwardCount);

  //backwardPeriod = micros() - backwardPrevTime;
  //Serial.print("Backward period: ");
  //Serial.println(backwardPeriod);
  //backwardPrevTime = micros();
}

// Moves in desired direction
// F: forward direction
// R: right direction
// L: left direction
// B: backward direction
// J: clockwise direction
// K: counterclockwise direction
// C: center the robot on the field
void omniControl(unsigned char robot_speed, char dir)
{

  switch(dir){

  case 'F':
    moveForward(robot_speed);
    break;

  case 'R':
    moveRight(robot_speed);
    break;

  case 'L':
    moveLeft(robot_speed);
    break;

  case 'B':
    moveBackward(robot_speed);
    break;

  case 'J':
    moveClockwise(robot_speed);
    break;

  case 'K':
    moveCounterClockwise(robot_speed);
    break;

  case 'C':
    centerRobot(robot_speed);
    break;
  }
}

void moveForward(unsigned char robot_speed)
{
   motor1_speed = (float(robot_speed)/float(sqrt(3)))*2;
   motor2_speed = (float(robot_speed)/float(sqrt(3)))*2;
   motor3_speed = 0;

   motor1_speed = int(motor1_speed);
   motor2_speed = int(motor2_speed);
   motor3_speed = int(motor3_speed);

   int cycle_time = 1;
   float left_period = 0, right_period = 0;
   static unsigned long prev_leftCount = 0, prev_rightCount = 0;
   unsigned long diff_leftCount = 0, diff_rightCount = 0;

   diff_leftCount = leftCount - prev_leftCount;
   prev_leftCount = leftCount;

   diff_rightCount = rightCount - prev_rightCount;
   prev_rightCount = rightCount;

   if (diff_rightCount != 0 && diff_leftCount != 0)
   {
     left_period = cycle_time / (float) diff_leftCount;
     right_period = cycle_time / (float) diff_rightCount;
   }

   proportional_ = left_period - right_period;
   derivative_ = proportional_ - last_proportional_;
   integral_ += proportional_;
   last_proportional_ = proportional_;

   error_ = Kp_ * proportional_ + Kd_ * derivative_ + Ki_ * integral_;

   motor2_speed = motor2_speed + error_;
   motor1_speed = motor1_speed - error_;

   if (motor2_speed > MAX_PWM)
     motor2_speed = MAX_PWM;

   if (motor2_speed < 0)
     motor2_speed = 0;

   if (motor1_speed > MAX_PWM)
     motor1_speed = MAX_PWM;

   if (motor1_speed < 0)
     motor1_speed = 0;

   motor_right(motor1_speed, 1);
   motor_left(motor2_speed, 0);
   motor_backward(motor3_speed,1);
}

void moveRight(unsigned char robot_speed)
{
  motor1_speed = robot_speed * 2;
  motor2_speed = robot_speed * 2;
  motor3_speed = robot_speed;

  motor1_speed = int(motor1_speed);
  motor2_speed = int(motor2_speed);
  motor3_speed = int(motor3_speed);

  int cycle_time = 1;
  float left_period = 0, right_period = 0;
  float backward_period = 0;
  static unsigned long prev_leftCount = 0, prev_rightCount = 0;
  static unsigned long prev_backwardCount = 0;
  unsigned long diff_leftCount = 0, diff_rightCount = 0;
  unsigned long diff_backwardCount = 0;

  diff_leftCount = leftCount - prev_leftCount;
  prev_leftCount = leftCount;

  diff_rightCount = rightCount - prev_rightCount;
  prev_rightCount = rightCount;

  diff_backwardCount = backwardCount - prev_backwardCount;
  prev_backwardCount = backwardCount;

  if (diff_rightCount != 0 && diff_leftCount != 0 && diff_backwardCount != 0)
  {
    left_period = cycle_time / (float) diff_leftCount;
    right_period = cycle_time / (float) diff_rightCount;
    backward_period = cycle_time / (float) diff_backwardCount;
  }

  left_period *= 2;
  right_period *= 2;

  proportional_ = left_period - right_period;
  derivative_ = proportional_ - last_proportional_;
  integral_ += proportional_;
  last_proportional_ = proportional_;

  error_ = Kp_ * proportional_ + Kd_ * derivative_ + Ki_ * integral_;

  proportional__ = right_period - backward_period;
  derivative__ = proportional__ - last_proportional__;
  integral__ += proportional__;
  last_proportional__ = proportional__;

  error__ = Kp__ * proportional__ + Kd__ * derivative__ + Ki__ * integral__;

  proportional___ = left_period - backward_period;
  derivative___ = proportional___ - last_proportional___;
  integral___ += proportional___;
  last_proportional___ = proportional___;

  error___ = Kp___ * proportional___ + Kd___ * derivative___ + Ki___ * integral___;

  motor2_speed = motor2_speed + error_ + error___;
  motor1_speed = motor1_speed - error_ + error__;
  motor3_speed = motor3_speed - error__ - error___;

  if (motor2_speed > MAX_PWM)
    motor2_speed = MAX_PWM;

  if (motor2_speed < 0)
    motor2_speed = 0;

  if (motor1_speed > MAX_PWM)
    motor1_speed = MAX_PWM;

  if (motor1_speed < 0)
    motor1_speed = 0;

  if (motor3_speed > MAX_PWM)
    motor3_speed = MAX_PWM;

  if (motor3_speed < 0)
    motor3_speed = 0;

    motor_right(motor1_speed, 0);
    motor_left(motor2_speed, 0);
    motor_backward(motor3_speed,1);
}

void moveLeft(unsigned char robot_speed)
{
  motor1_speed = robot_speed * 2;
  motor2_speed = robot_speed * 2;
  motor3_speed = robot_speed;

  motor1_speed = int(motor1_speed);
  motor2_speed = int(motor2_speed);
  motor3_speed = int(motor3_speed);

  int cycle_time = 1;
  float left_period = 0, right_period = 0;
  float backward_period = 0;
  static unsigned long prev_leftCount = 0, prev_rightCount = 0;
  static unsigned long prev_backwardCount = 0;
  unsigned long diff_leftCount = 0, diff_rightCount = 0;
  unsigned long diff_backwardCount = 0;

  diff_leftCount = leftCount - prev_leftCount;
  prev_leftCount = leftCount;

  diff_rightCount = rightCount - prev_rightCount;
  prev_rightCount = rightCount;

  diff_backwardCount = backwardCount - prev_backwardCount;
  prev_backwardCount = backwardCount;

  if (diff_rightCount != 0 && diff_leftCount != 0 && diff_backwardCount != 0)
  {
    left_period = cycle_time / (float) diff_leftCount;
    right_period = cycle_time / (float) diff_rightCount;
    backward_period = cycle_time / (float) diff_backwardCount;
  }

  left_period *= 2;
  right_period *= 2;

  proportional_ = left_period - right_period;
  derivative_ = proportional_ - last_proportional_;
  integral_ += proportional_;
  last_proportional_ = proportional_;

  error_ = Kp_ * proportional_ + Kd_ * derivative_ + Ki_ * integral_;

  proportional__ = right_period - backward_period;
  derivative__ = proportional__ - last_proportional__;
  integral__ += proportional__;
  last_proportional__ = proportional__;

  error__ = Kp__ * proportional__ + Kd__ * derivative__ + Ki__ * integral__;

  proportional___ = left_period - backward_period;
  derivative___ = proportional___ - last_proportional___;
  integral___ += proportional___;
  last_proportional___ = proportional___;

  error___ = Kp___ * proportional___ + Kd___ * derivative___ + Ki___ * integral___;

  motor2_speed = motor2_speed + error_ + error___;
  motor1_speed = motor1_speed - error_ + error__;
  motor3_speed = motor3_speed - error__ - error___;

  if (motor2_speed > MAX_PWM)
    motor2_speed = MAX_PWM;

  if (motor2_speed < 0)
    motor2_speed = 0;

  if (motor1_speed > MAX_PWM)
    motor1_speed = MAX_PWM;

  if (motor1_speed < 0)
    motor1_speed = 0;

  if (motor3_speed > MAX_PWM)
    motor3_speed = MAX_PWM;

  if (motor3_speed < 0)
    motor3_speed = 0;

    motor_right(motor1_speed, 1);
    motor_left(motor2_speed, 1);
    motor_backward(motor3_speed,0);
}

void moveBackward(unsigned char robot_speed)
{
   motor1_speed = (float(robot_speed)/float(sqrt(3)))*2;
   motor2_speed = (float(robot_speed)/float(sqrt(3)))*2;
   motor3_speed = 0;

   motor1_speed = int(motor1_speed);
   motor2_speed = int(motor2_speed);
   motor3_speed = int(motor3_speed);

   int cycle_time = 1;
   float left_period = 0, right_period = 0;
   static unsigned long prev_leftCount = 0, prev_rightCount = 0;
   unsigned long diff_leftCount = 0, diff_rightCount = 0;

   diff_leftCount = leftCount - prev_leftCount;
   prev_leftCount = leftCount;

   diff_rightCount = rightCount - prev_rightCount;
   prev_rightCount = rightCount;

   if (diff_rightCount != 0 && diff_leftCount != 0)
   {
     left_period = cycle_time / (float) diff_leftCount;
     right_period = cycle_time / (float) diff_rightCount;
   }

   proportional_ = left_period - right_period;
   derivative_ = proportional_ - last_proportional_;
   integral_ += proportional_;
   last_proportional_ = proportional_;

   error_ = Kp_ * proportional_ + Kd_ * derivative_ + Ki_ * integral_;

   motor2_speed = motor2_speed + error_;
   motor1_speed = motor1_speed - error_;

   if (motor2_speed > MAX_PWM)
     motor2_speed = MAX_PWM;

   if (motor2_speed < 0)
     motor2_speed = 0;

   if (motor1_speed > MAX_PWM)
     motor1_speed = MAX_PWM;

   if (motor1_speed < 0)
     motor1_speed = 0;

   motor_right(motor1_speed, 0);
   motor_left(motor2_speed, 1);
   motor_backward(motor3_speed,1);
}

void moveClockwise(unsigned char robot_speed)
{
  motor1_speed = robot_speed;
  motor2_speed = robot_speed;
  motor3_speed = robot_speed;

  motor1_speed = int(motor1_speed);
  motor2_speed = int(motor2_speed);
  motor3_speed = int(motor3_speed);

  int cycle_time = 1;
  float left_period = 0, right_period = 0;
  float backward_period = 0;
  static unsigned long prev_leftCount = 0, prev_rightCount = 0;
  static unsigned long prev_backwardCount = 0;
  unsigned long diff_leftCount = 0, diff_rightCount = 0;
  unsigned long diff_backwardCount = 0;

  diff_leftCount = leftCount - prev_leftCount;
  prev_leftCount = leftCount;

  diff_rightCount = rightCount - prev_rightCount;
  prev_rightCount = rightCount;

  diff_backwardCount = backwardCount - prev_backwardCount;
  prev_backwardCount = backwardCount;

  if (diff_rightCount != 0 && diff_leftCount != 0 && diff_backwardCount != 0)
  {
    left_period = cycle_time / (float) diff_leftCount;
    right_period = cycle_time / (float) diff_rightCount;
    backward_period = cycle_time / (float) diff_backwardCount;
  }

  proportional_ = left_period - right_period;
  derivative_ = proportional_ - last_proportional_;
  integral_ += proportional_;
  last_proportional_ = proportional_;

  error_ = Kp_ * proportional_ + Kd_ * derivative_ + Ki_ * integral_;

  proportional__ = right_period - backward_period;
  derivative__ = proportional__ - last_proportional__;
  integral__ += proportional__;
  last_proportional__ = proportional__;

  error__ = Kp__ * proportional__ + Kd__ * derivative__ + Ki__ * integral__;

  proportional___ = left_period - backward_period;
  derivative___ = proportional___ - last_proportional___;
  integral___ += proportional___;
  last_proportional___ = proportional___;

  error___ = Kp___ * proportional___ + Kd___ * derivative___ + Ki___ * integral___;

  motor2_speed = motor2_speed + error_ + error___;
  motor1_speed = motor1_speed - error_ + error__;
  motor3_speed = motor3_speed - error__ - error___;

  if (motor2_speed > MAX_PWM)
    motor2_speed = MAX_PWM;

  if (motor2_speed < 0)
    motor2_speed = 0;

  if (motor1_speed > MAX_PWM)
    motor1_speed = MAX_PWM;

  if (motor1_speed < 0)
    motor1_speed = 0;

  if (motor3_speed > MAX_PWM)
    motor3_speed = MAX_PWM;

  if (motor3_speed < 0)
    motor3_speed = 0;

    motor_right(motor1_speed, 0);
    motor_left(motor2_speed, 0);
    motor_backward(motor3_speed,0);
}

void moveCounterClockwise(unsigned char robot_speed)
{
  motor1_speed = robot_speed;
  motor2_speed = robot_speed;
  motor3_speed = robot_speed;

  motor1_speed = int(motor1_speed);
  motor2_speed = int(motor2_speed);
  motor3_speed = int(motor3_speed);

  int cycle_time = 1;
  float left_period = 0, right_period = 0;
  float backward_period = 0;
  static unsigned long prev_leftCount = 0, prev_rightCount = 0;
  static unsigned long prev_backwardCount = 0;
  unsigned long diff_leftCount = 0, diff_rightCount = 0;
  unsigned long diff_backwardCount = 0;

  diff_leftCount = leftCount - prev_leftCount;
  prev_leftCount = leftCount;

  diff_rightCount = rightCount - prev_rightCount;
  prev_rightCount = rightCount;

  diff_backwardCount = backwardCount - prev_backwardCount;
  prev_backwardCount = backwardCount;

  if (diff_rightCount != 0 && diff_leftCount != 0 && diff_backwardCount != 0)
  {
    left_period = cycle_time / (float) diff_leftCount;
    right_period = cycle_time / (float) diff_rightCount;
    backward_period = cycle_time / (float) diff_backwardCount;
  }

  proportional_ = left_period - right_period;
  derivative_ = proportional_ - last_proportional_;
  integral_ += proportional_;
  last_proportional_ = proportional_;

  error_ = Kp_ * proportional_ + Kd_ * derivative_ + Ki_ * integral_;

  proportional__ = right_period - backward_period;
  derivative__ = proportional__ - last_proportional__;
  integral__ += proportional__;
  last_proportional__ = proportional__;

  error__ = Kp__ * proportional__ + Kd__ * derivative__ + Ki__ * integral__;

  proportional___ = left_period - backward_period;
  derivative___ = proportional___ - last_proportional___;
  integral___ += proportional___;
  last_proportional___ = proportional___;

  error___ = Kp___ * proportional___ + Kd___ * derivative___ + Ki___ * integral___;

  motor2_speed = motor2_speed + error_ + error___;
  motor1_speed = motor1_speed - error_ + error__;
  motor3_speed = motor3_speed - error__ - error___;

  if (motor2_speed > MAX_PWM)
    motor2_speed = MAX_PWM;

  if (motor2_speed < 0)
    motor2_speed = 0;

  if (motor1_speed > MAX_PWM)
    motor1_speed = MAX_PWM;

  if (motor1_speed < 0)
    motor1_speed = 0;

  if (motor3_speed > MAX_PWM)
    motor3_speed = MAX_PWM;

  if (motor3_speed < 0)
    motor3_speed = 0;

    motor_right(motor1_speed, 1);
    motor_left(motor2_speed, 1);
    motor_backward(motor3_speed,1);
}

void centerRobot(unsigned char robot_speed)
{
  int right_dist, left_dist;

  right_dist = sonar[1].ping_cm();
  delay(30);
  left_dist = sonar[2].ping_cm();
  delay(30);

  while(((right_dist - left_dist) > 5) || ((left_dist - right_dist) > 5))
  {
    if((right_dist - left_dist) > 5)
    moveRight(robot_speed);
    if((left_dist - right_dist) > 5)
    moveLeft(robot_speed);
  }

  stopRobot();
}
